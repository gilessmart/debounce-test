describe('Main Controller', function (){
    'use strict';

    var controller,
        scope,
        dataServiceMock,
        getPricingDataResult;

    beforeEach(module('debounce-test'));

    beforeEach(inject(function ($injector) {
        var $controller = $injector.get('$controller'),
            $rootScope = $injector.get('$rootScope'),
            $q = $injector.get('$q');

        scope = $rootScope.$new();
        
        dataServiceMock = jasmine.createSpyObj('data', ['getPricingData']);
        dataServiceMock.getPricingData.and.callFake(function () {
            return $q.when(getPricingDataResult);
        });
        
        var debouncerMock = jasmine.createSpyObj('debouncer', ['create']);
        debouncerMock.create.and.callFake(function (callback) { 
            return callback; 
        });
        
        controller = $controller('mainController as vm', { 
            $scope: scope,
            dataService: dataServiceMock,
            debouncer: debouncerMock
        });
    }));

    describe('When onQuantityChanged is called', function () {
        beforeEach(function () {
            getPricingDataResult = { totalPrice: "£10.00" };
            controller.onQuantityChanged(2);
            scope.$digest();
        });

        it('A call is made to the service', function () {
            expect(dataServiceMock.getPricingData).toHaveBeenCalledWith(2);
        });
        
        it('Total price is set', function () {
            expect(controller.totalPrice).toBe("£10.00");
        });
    })
});