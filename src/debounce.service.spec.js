describe('debouncer', function () {
    'use strict';

    var $timeout,
        debouncer;

    beforeEach(module('debounce-test'));

    beforeEach(inject(function ($injector) {
        $timeout = $injector.get('$timeout');
        debouncer = $injector.get('debouncer');
    }));

    describe('When created', function () {
        var debounced,
            callback;

        beforeEach(function () {
            callback = jasmine.createSpy('callback');
            debounced = debouncer.create(callback, 250);
        });

        it('Creates a function', function () {
            expect(typeof debounced).toBe('function');
        });

        describe('When the debounced function is invoked', function () {
            beforeEach(function () {
                debounced('a', 1, false);
            });

            it('The callback is NOT invoked', function () {
                expect(callback).not.toHaveBeenCalled();
            });

            describe('Then the timeout expires', function () {
                beforeEach(function () {
                    $timeout.flush();
                });

                it('The callback is invoked once', function () {
                    expect(callback.calls.count()).toBe(1);
                });

                it('The callback is invoked with the arguments passed to the debouced function', function() {
                    expect(callback).toHaveBeenCalledWith('a', 1, false);
                });
            });
            
            describe('Then called again', function () {
                beforeEach(function () {
                    debounced('b', 2, true);
                });

                describe('Then the timeout expires', function () {
                    beforeEach(function () {
                        $timeout.flush();
                    });
                    
                    it('The callback is invoked once', function () {
                        expect(callback.calls.count()).toBe(1);
                    });

                    it('The callback is invoked with the 2nd set of arguments passed to the debouced function', function() {
                        expect(callback).toHaveBeenCalledWith('b', 2, true);
                    });
                });                
            });
        })
    });
});