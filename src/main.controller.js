(function () {
    angular
        .module('debounce-test')
        .controller('mainController', MainController);

    function MainController (dataService, debouncer) {
        var vm = this;
        vm.onQuantityChanged = debouncer.create(onQuantityChanged, 250);
        vm.totalPrice = null;

        function onQuantityChanged(newQuantity) {
            dataService.getPricingData(newQuantity).then(function (pricingData) {
                vm.totalPrice = pricingData.totalPrice;
            });                
        }
    }
})();

