(function () {
    angular
        .module('debounce-test')
        .factory('debouncer', debouncer);

    function debouncer ($timeout) {
        return {
            create: create
        };

        function create (callback, delay) {
            var previousTimeout;

            return function () {
                var callerContext = this,
                    callerArguments = arguments;

                $timeout.cancel(previousTimeout);

                previousTimeout = $timeout(function () {
                    callback.apply(callerContext, callerArguments);
                }, delay);
            };
        }
    }
})();
